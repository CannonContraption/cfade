default: cfade.out

cfade.out: cfade.c
	gcc -o cfade.out cfade.c -O0

install: cfade.out
	cp cfade.out ~/.local/bin/
