#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>

/*
  Welcome to the CFade source code!
  
  Here you'll see source code for fading text written in C!

  Written January 2019 by Jim Read.
*/

#define LIMIT1 255
#define LIMIT2 236

#define DELAY_SECONDS 0
#define DELAY_NANOSEC 100

/*
  Program-wide switches
*/
//#define PLAY_BACKGROUND

/*
  The essential idea we're working with builds on the ANSI color escapes for
  greyscale values. If we use [38;5;(232-255)m we get a range of greys in the
  foreground. If we sub s/38/48/ in the above statement, we get background greys
  
  This struct is just to track that for each character.
*/
struct character
{
  char value;
  int  foreground;
  int  background;
};

/*
  get_single_line
  
  struct character ** allchars
      Pointer to an array location for allchars
      
  get_single_line is designed to read a single line from stdin and put its
  characters into a struct we can use.
*/
int get_single_line(
    struct character ** allchars)
{
  /*
    Basic local variables
  */
  char stdinline[512];
  int  char_no;
  int  line_len;

  /*
    And now the actual code to read a line.
    
    Use of fgets here is because C11 deprecated gets and C14 removed it. This
    was because gets had no way to receive information on buffer size, and would
    cause overflows as a result.
  */
  fgets(
      stdinline,
      sizeof(stdinline),
      stdin);
  line_len = strlen(stdinline);

  /* We need an array to hold our characters. Every one is independant. */
  (*allchars) = malloc(
      sizeof(struct character) * line_len);

  /* Then we populate the array with some defaults. */
  for(
      char_no = 0;
      char_no < line_len;
      char_no ++)
    {
      (*allchars)[char_no].value = stdinline[char_no];
      (*allchars)[char_no].foreground = LIMIT2;
      (*allchars)[char_no].background = LIMIT1;
    }
  return line_len;
}

/*
  print_clear_line
  
  struct character * allchars
      Array of all characters we want to print
  int allchars_len
      Length of the above array (because laziness?)

  Prints a line of text with all ANSI color sequences reset (cleared)
*/
void print_clear_line(
    struct character * allchars,
    int                allchars_len)
{
  int             char_id;
  struct timespec requested_time;
  struct timespec remaining_time;
  /*
    Since they could all be individually escaped, we go through the whole line
    and redo the escape codes character by character.
  */
  for(
      char_id = 0;
      char_id < allchars_len;
      char_id ++)
    {
      if(allchars[char_id].value == '\n')
        {
          continue;
        }
      printf(
          "[m%c",
          allchars[char_id].value);
      /*
        This entire section is just to make the delay work right.

        Basically we need a sleep .001 or something like that and this gets the
        job done.
      */
      requested_time.tv_nsec = DELAY_NANOSEC;
      requested_time.tv_sec  = DELAY_SECONDS;
      nanosleep(
          &requested_time,
          &remaining_time);
    }
}

/*
  print_line
  
  struct character * allchars
      Array of all characters we want to print
  int allchars_len
      Length of the above array
  
  Prints a line of text with the set color settings for the fade routine. This
  is built to be drawn over itself a few times to create the final result.
*/
void print_line(
    struct character * allchars,
    int                allchars_len)
{
  int             char_id;
  struct timespec requested_time;
  struct timespec remaining_time;
  /*
    Since we're dealing with individual escape sequences, we need to run through
    this one character at a time.
  */
  for(
      char_id = 0;
      char_id < allchars_len;
      char_id ++)
    {
      /* Skips new line characters since they screw up the fade-in animation */
      if(allchars[char_id].value == '\n')
        {
          continue;
        }
      /*
        We have two cases to handle. One where we bother with the background
        color, one where we don't.
      */
      #ifdef PLAY_BACKGROUND
      printf(
          "[38;5;%dm[48;5;%dm%c",
          allchars[char_id].foreground,
          allchars[char_id].background,
          allchars[char_id].value);
      #else
      printf(
          "[38;5;%dm%c",
          allchars[char_id].foreground,
          allchars[char_id].value);
      #endif
      /*
        This entire section is just to make the delay work right.

        Basically we need a sleep .001 or something like that and this gets the
        job done.
      */
      requested_time.tv_nsec = DELAY_NANOSEC;
      requested_time.tv_sec  = DELAY_SECONDS;
      nanosleep(
          &requested_time,
          &remaining_time);
    }
}

/*
  play_forward
  
  struct character * allchars
      Array of all characters we want to animate
  int allchars_len
      Length of the above array
  
  Plays the animation in the forwards direction, followed by a clear print pass
  to get rid of visual residue from the last line printed.
*/
int play_forward(
    struct character * allchars,
    int                allchars_len)
{
  assert(allchars != NULL);
  /*
    The basic idea behind this effect is it is a cascade effect of sorts. It
    would follow the following pattern for four characters with four steps:
    0 0 0 0
    1 0 0 0
    2 1 0 0
    3 2 1 0
    3 3 2 1
    3 3 3 2
    3 3 3 3
  */

  /* Number of characters we've touched so far */
  int  no_to_increment;
  /* ID value for the character we're messing with right now */
  int  char_id;

  /*
    Our next step is to increment through each character and increment them if
    we've gotten that far until the character reaches its max color setting.
  */
  for(
      no_to_increment = 1;
      allchars[allchars_len - 1].foreground < LIMIT1;
      no_to_increment ++)
    {
      /* Each iteration we start the line over. */
      printf("\r");
      /* Since we're about to ref characters, make sure we're in bounds */
      if(no_to_increment > allchars_len)
        {
          no_to_increment = allchars_len;
        }
      /* Then we do the actual inrement work */
      for(
          char_id = 0;
          char_id < no_to_increment;
          char_id ++)
        {
          if(allchars[char_id].foreground < LIMIT1)
            {
              allchars[char_id].foreground ++;
              allchars[char_id].background --;
            }
        }
      /* Then we print the result of this pass. */
      print_line(
          allchars,
          allchars_len);
    }
  /* Now that the animation is done, print the characters as if they hadn't
     been animated at all */
  printf("\r");
  print_clear_line(
      allchars,
      allchars_len);
  return 0;
}

/*
  And then main() just glues everything together and adds multi-line support.
*/
int main()
{
  /*
    We can stick with just one pointer for now.
  */
  struct character * allchars = NULL;
  /*
    And then we go through and read all the lines we get, running the animation
    on each one.
  */
  /* We run this first to prevent superfluous if statements for EOF */
  int line_len = get_single_line(
      &allchars);
  while(!feof(stdin))
    {
      /* Play the animation once */
      play_forward(
          allchars,
          line_len);
      /* Break the line and try to avoid memory leaks */
      printf("\n");
      free(allchars);
      allchars = NULL;
      /* Read the next line, if it exists */
      line_len = get_single_line(
          &allchars);
    }
  return 0;
}
